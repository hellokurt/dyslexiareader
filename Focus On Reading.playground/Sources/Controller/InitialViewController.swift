import UIKit
import PlaygroundSupport
import AVFoundation

let books = Books()

public class InitialViewController: UIViewController {
    
    var titleOfTheChapter: UITextView!
    var textOfTheBook: UITextView!
    var adjustFontButton: UIButton!
    var minimizeFontButton: UIButton!
    var lineSpacingButton: UIButton!
    var bgPastelColorButton: UIButton!
    var bgWhiteOnBlackColorButton: UIButton!
    var bgLowVisionColorButton: UIButton!
    var wordSpacingAdjustButton: UIButton!
    var spaceMinimizerButton: UIButton!
    var fontToHelvetica: UIButton!
    var dyslexicFontButton: UIButton!
    var leftMarginButton: UIButton!
    var settingsButton: UIButton!
    var adjuster: CGFloat!
    var counter = 0
    var counterBlackWhite = 0
    var counterLowVision = 0
    var wordSpacing = 0
    var fontName: String = ""
    var size: CGFloat = 0.0
    var margin = 50
    let speechSynthesizer = AVSpeechSynthesizer()
    var tappedWord: String!
    
    
    //interface setup functions
    
    private func setTitleOfChapter(){
        titleOfTheChapter = UITextView()
        
        titleOfTheChapter.text = books.chapterOfTheBook[0]
        titleOfTheChapter.frame = CGRect(x: 50, y: 125, width: 600, height: 80)
        titleOfTheChapter.textAlignment = .center
//        titleOfTheChapter.font = UIFont.preferredFont(forTextStyle: .headline)
        titleOfTheChapter.font = UIFont(name: fontName, size: adjuster)
        titleOfTheChapter.textColor = .black
        titleOfTheChapter.isSelectable = false
        titleOfTheChapter.isEditable = false
        titleOfTheChapter.backgroundColor = #colorLiteral(red: 1, green: 0.9224860072, blue: 0.8000084758, alpha: 1)
    }
    private func setTextOfTheBook(){
        textOfTheBook = UITextView()
        textOfTheBook.text = books.textOfTheChapter[0]
        textOfTheBook.sizeToFit()
        textOfTheBook.frame = CGRect(x: 50, y: 210, width: 610, height: 460)
        textOfTheBook.font = UIFont.preferredFont(forTextStyle: .body)
        textOfTheBook.font = UIFont(name:fontName, size: adjuster)
        textOfTheBook.textColor = .black
        textOfTheBook.isEditable = false
        textOfTheBook.isSelectable = true
        textOfTheBook.backgroundColor = #colorLiteral(red: 1, green: 0.9224860072, blue: 0.8000084758, alpha: 1)
        textOfTheBook.isScrollEnabled = true
        textOfTheBook.showsVerticalScrollIndicator = true
        textOfTheBook.isPagingEnabled = true
        textOfTheBook.bounces = true
    }
    private func setButtonFontAdjuster(){
        adjustFontButton = UIButton()
        adjustFontButton.setBackgroundImage(#imageLiteral(resourceName: "adjustFontBlackNormal.png"), for: .normal)
        adjustFontButton.setBackgroundImage(#imageLiteral(resourceName: "adjustFontBlack.png"), for: .highlighted)
        adjustFontButton.frame = CGRect(x: 120, y: 80, width: 70, height: 50)
        adjustFontButton.isUserInteractionEnabled = true
        adjustFontButton.accessibilityLabel = "Increase font size"
        
    }
    private func setButtonForFontReducing(){
        minimizeFontButton = UIButton()
        minimizeFontButton.setBackgroundImage(#imageLiteral(resourceName: "reduceFontBlackNormal.png"), for: .normal)
        minimizeFontButton.setBackgroundImage(#imageLiteral(resourceName: "reduceFontBlack.png"), for: .highlighted)
        minimizeFontButton.frame = CGRect(x: 30, y: 80, width: 70, height: 50)
        minimizeFontButton.isUserInteractionEnabled = true
        minimizeFontButton.accessibilityLabel = "Decrease font size"
    }
    private func setButtonWordSpacingAdjustment(){
        wordSpacingAdjustButton = UIButton()
        wordSpacingAdjustButton.setBackgroundImage(#imageLiteral(resourceName: "adjustWordSpacingNormal.png"), for: .normal)
        wordSpacingAdjustButton.setBackgroundImage(#imageLiteral(resourceName: "adjustWordSpacingBlack.png"), for: .highlighted)
        wordSpacingAdjustButton.frame = CGRect(x: 300, y: 80, width: 70, height: 50)
        wordSpacingAdjustButton.isUserInteractionEnabled = true
        wordSpacingAdjustButton.accessibilityLabel = "Increase word spacing"
    }
    private func setButtonWordSpacingReducing(){
        spaceMinimizerButton = UIButton()
        spaceMinimizerButton.setBackgroundImage(#imageLiteral(resourceName: "reduceWordSpacingBlackNormal.png"), for: .normal)
        spaceMinimizerButton.setBackgroundImage(#imageLiteral(resourceName: "reduceWordSpacingBlack.png"), for: .highlighted)
        spaceMinimizerButton.frame = CGRect(x: 210, y: 80, width: 70, height: 50)
        spaceMinimizerButton.isUserInteractionEnabled = true
        spaceMinimizerButton.accessibilityLabel = "Decrease word spacing"
    }
    private func setLeftMarginButton(){
        leftMarginButton = UIButton()
        leftMarginButton.setBackgroundImage(#imageLiteral(resourceName: "marginLeftBlack.png"), for: .normal)
        leftMarginButton.frame = CGRect(x: 480, y: 20, width: 50, height: 50)
        leftMarginButton.isUserInteractionEnabled = true
        leftMarginButton.accessibilityLabel = "Change left margin"
    }
    private func setLineSpacingButton(){
        lineSpacingButton = UIButton()
        lineSpacingButton.setBackgroundImage(#imageLiteral(resourceName: "lineSpacingBlack.png"), for: .normal)
        lineSpacingButton.frame = CGRect(x: 420, y: 20, width: 50, height: 50)
        lineSpacingButton.accessibilityLabel = "Change line spacing"
    }
    private func setBgPastelColor(){
        bgPastelColorButton = UIButton()
        bgPastelColorButton.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.8196078431, blue: 0.6901960784, alpha: 1)
        bgPastelColorButton.frame = CGRect(x: 30, y: 20, width: 50, height: 50)
        bgPastelColorButton.accessibilityLabel = "Dyslexia friendly backgrounds"
    }
    private func setBgLowVisionColor(){
        bgLowVisionColorButton = UIButton()
        bgLowVisionColorButton.backgroundColor = .yellow
        bgLowVisionColorButton.frame = CGRect(x: 90, y: 20, width: 50, height: 50)
        bgLowVisionColorButton.accessibilityLabel = "Low vision background"
    }
    private func setBgWhiteOnBlack(){
        bgWhiteOnBlackColorButton = UIButton()
        bgWhiteOnBlackColorButton.backgroundColor = .black
        bgWhiteOnBlackColorButton.frame = CGRect(x: 150, y: 20, width: 50, height: 50)
        bgWhiteOnBlackColorButton.accessibilityLabel = "Accessible Black & White background"
    }
    private func setFontToHelvetica(){
        fontToHelvetica = UIButton()
        fontToHelvetica.setBackgroundImage(#imageLiteral(resourceName: "neuHelveticaBlack.png"), for: .normal)
        fontToHelvetica.frame = CGRect(x: 350, y: 20, width: 50, height: 50)
        fontToHelvetica.accessibilityLabel = "Helvetica font"
    }
    private func setDyslexicFontButton(){
        dyslexicFontButton = UIButton()
        dyslexicFontButton.frame = CGRect(x: 280, y: 20, width: 50, height: 50)
        dyslexicFontButton.setBackgroundImage(#imageLiteral(resourceName: "dyslexiaBlack.png"), for: .normal)
        dyslexicFontButton.accessibilityLabel = "Dyslexia font"
    }
    private func setSettingsIcon(){
        settingsButton = UIButton()
        settingsButton.frame = CGRect(x: 550, y: 20, width: 50, height: 50)
        settingsButton.setBackgroundImage(#imageLiteral(resourceName: "settingsBlack.png"), for: .normal)
        settingsButton.isAccessibilityElement = true
        settingsButton.accessibilityLabel = "Setting button"
        
    }
    public func setUIElements(){
        setTitleOfChapter()
        setTextOfTheBook()
        setButtonFontAdjuster()
        setButtonForFontReducing()
        setButtonWordSpacingAdjustment()
        setButtonWordSpacingReducing()
        setBgPastelColor()
        setFontToHelvetica()
        setDyslexicFontButton()
        setSettingsIcon()
        setBgWhiteOnBlack()
        setBgLowVisionColor()
        setLineSpacingButton()
        setLeftMarginButton()
    }
    public func setButtonInteractions(){
        let tapWord = UITapGestureRecognizer(target: self, action: #selector(tapWordResponse(recognizer:)))
        let tapWordInTitle = UITapGestureRecognizer(target: self, action: #selector(tapWordInTitleResponse(recognizer:)))
        let tap = UITapGestureRecognizer (target:self, action: #selector(tappedIncreaseFontSize))
        let tapRed = UITapGestureRecognizer (target:self, action: #selector(tappedDecreaseFontSize))
        let tapBgChanger = UITapGestureRecognizer (target:self, action: #selector(setDyslexiaPastelBackground))
        let tapSpaceAdjuster = UITapGestureRecognizer (target:self, action: #selector(tappedWordSpacingIncreaseButton))
        let tapSpaceMinimizer = UITapGestureRecognizer(target:self, action: #selector(tappedWordSpacingDecreaseButton))
        let tapHelveticaFont = UITapGestureRecognizer(target:self, action: #selector(tappedHelveticaFontButton))
        let tapOpenDislexiaFont = UITapGestureRecognizer(target:self, action: #selector(tappedOpenDislexicFontButton))
        let showSetting = UITapGestureRecognizer(target:self, action: #selector(tappedSettings))
        let lowVisionTap = UITapGestureRecognizer(target:self, action: #selector(setLowVisionBackground))
        let blackOnWhiteTap = UITapGestureRecognizer(target:self, action: #selector(setOffBlackAndOffWhiteBackground))
        let lineSpacerTap = UITapGestureRecognizer(target:self, action: #selector(tappedLineSpacer))
        let leftMarginButtonTap = UITapGestureRecognizer(target:self, action: #selector(tappedLeftMarginButton))
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(doubleTapTouch(recognizer:)))
        doubleTap.numberOfTouchesRequired = 2
        
        textOfTheBook.addGestureRecognizer(doubleTap)
        adjustFontButton.addGestureRecognizer(tap)
        titleOfTheChapter.addGestureRecognizer(tapWordInTitle)
        minimizeFontButton.addGestureRecognizer(tapRed)
        textOfTheBook.addGestureRecognizer(tapWord)
        bgPastelColorButton.addGestureRecognizer(tapBgChanger)
        wordSpacingAdjustButton.addGestureRecognizer(tapSpaceAdjuster)
        spaceMinimizerButton.addGestureRecognizer(tapSpaceMinimizer)
        fontToHelvetica.addGestureRecognizer(tapHelveticaFont)
        dyslexicFontButton.addGestureRecognizer(tapOpenDislexiaFont)
        settingsButton.addGestureRecognizer(showSetting)
        bgLowVisionColorButton.addGestureRecognizer(lowVisionTap)
        lineSpacingButton.addGestureRecognizer(lineSpacerTap)
        bgWhiteOnBlackColorButton.addGestureRecognizer(blackOnWhiteTap)
        leftMarginButton.addGestureRecognizer(leftMarginButtonTap)
    }
    
    // create and load elements of the view
    
    public override func loadView() {
        let fontURL = Bundle.main.url(forResource: "OpenDyslexic-Regular", withExtension: "otf")
        CTFontManagerRegisterFontsForURL(fontURL! as CFURL, CTFontManagerScope.process, nil)
        
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 1, green: 0.9215686275, blue: 0.8, alpha: 1)
        
        
        fontName = "OpenDyslexic-Regular"
        adjuster = 18.0
        size = 10.0
        
        setUIElements()
        setButtonInteractions()
        operationalControlHiding()

        view.addSubview(titleOfTheChapter)
        view.addSubview(textOfTheBook)
        view.addSubview(adjustFontButton)
        view.addSubview(minimizeFontButton)
        view.addSubview(wordSpacingAdjustButton)
        view.addSubview(spaceMinimizerButton)
        view.addSubview(bgPastelColorButton)
        view.addSubview(fontToHelvetica)
        view.addSubview(dyslexicFontButton)
        view.addSubview(settingsButton)
        view.addSubview(bgWhiteOnBlackColorButton)
        view.addSubview(bgLowVisionColorButton)
        view.addSubview(lineSpacingButton)
        view.addSubview(leftMarginButton)
        
        lineSpacing(of: size)
        initialTextColorHolder()
        
        self.view = view
    }
    
    // showing and hiding controls function
    
    @objc func tappedSettings(recognizer: UITapGestureRecognizer) {
        speechUtterance(tappedWord: settingsButton.accessibilityLabel!)
        
        if adjustFontButton.isHidden == false {
            operationalControlHiding()
        }
        else {
            wordSpacingAdjustButton.isHidden = false
            adjustFontButton.isHidden = false
            spaceMinimizerButton.isHidden = false
            minimizeFontButton.isHidden = false
            fontToHelvetica.isHidden = false
            bgWhiteOnBlackColorButton.isHidden = false
            bgPastelColorButton.isHidden = false
            bgLowVisionColorButton.isHidden = false
            dyslexicFontButton.isHidden = false
            lineSpacingButton.isHidden = false
            leftMarginButton.isHidden = false
        }
    }
       
    // Background color change functions
    
    @objc func setDyslexiaPastelBackground(){
        counter += 1
        if counter % 2 == 1 {
            view.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.8196078431, blue: 0.6901960784, alpha: 1)
            backgroundIconsNormalizer()
            titleOfTheChapter.textColor = .black
            textOfTheBook.textColor = .black
            bgPastelColorButton.backgroundColor =  #colorLiteral(red: 0.9294117647, green: 0.8666666667, blue: 0.431372549, alpha: 1)
            titleOfTheChapter.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.8196078431, blue: 0.6901960784, alpha: 1)
            textOfTheBook.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.8196078431, blue: 0.6901960784, alpha: 1)
            
        }
        else  {
            view.backgroundColor =  #colorLiteral(red: 0.9294117647, green: 0.8666666667, blue: 0.431372549, alpha: 1)
            backgroundIconsNormalizer()
            titleOfTheChapter.textColor = .black
            textOfTheBook.textColor = .black
            bgPastelColorButton.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.8196078431, blue: 0.6901960784, alpha: 1)
            titleOfTheChapter.backgroundColor =  #colorLiteral(red: 0.9294117647, green: 0.8666666667, blue: 0.431372549, alpha: 1)
            textOfTheBook.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.8666666667, blue: 0.431372549, alpha: 1)
        }
        if counter <= 3 {
            speechUtterance(tappedWord: bgPastelColorButton.accessibilityLabel!)
        }
    }
    @objc func setOffBlackAndOffWhiteBackground(){
        counterBlackWhite += 1
        
        if counterBlackWhite % 2 == 1 {
            view.backgroundColor = #colorLiteral(red: 0.03921568627, green: 0.03921568627, blue: 0.03921568627, alpha: 1)
            bgWhiteOnBlackColorButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 0.8980392157, alpha: 1)
            
            titleOfTheChapter.backgroundColor = #colorLiteral(red: 0.03921568627, green: 0.03921568627, blue: 0.03921568627, alpha: 1)
            textOfTheBook.backgroundColor = #colorLiteral(red: 0.03921568627, green: 0.03921568627, blue: 0.03921568627, alpha: 1)
            titleOfTheChapter.textColor = #colorLiteral(red: 1, green: 1, blue: 0.8980392157, alpha: 1)
            textOfTheBook.textColor = #colorLiteral(red: 1, green: 1, blue: 0.8980392157, alpha: 1)
            backgroundIconsNormalizer()
        }
        else  {
            view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 0.8980392157, alpha: 1)
            bgWhiteOnBlackColorButton.backgroundColor = #colorLiteral(red: 0.03921568627, green: 0.03921568627, blue: 0.03921568627, alpha: 1)
            
            titleOfTheChapter.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 0.8980392157, alpha: 1)
            textOfTheBook.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 0.8980392157, alpha: 1)
            titleOfTheChapter.textColor = #colorLiteral(red: 0.03921568627, green: 0.03921568627, blue: 0.03921568627, alpha: 1)
            textOfTheBook.textColor = #colorLiteral(red: 0.03921568627, green: 0.03921568627, blue: 0.03921568627, alpha: 1)
            backgroundIconsNormalizer()
        }
        if counterBlackWhite <= 3 {
            speechUtterance(tappedWord: bgWhiteOnBlackColorButton.accessibilityLabel!)
        }
    }
    @objc func setLowVisionBackground(){
        counterLowVision += 1
        
        if counterLowVision % 2 == 1 {
            view.backgroundColor = .yellow
            backgroundIconsNormalizer()
            bgLowVisionColorButton.backgroundColor = .black
            titleOfTheChapter.backgroundColor = .yellow
            textOfTheBook.backgroundColor = .yellow
            titleOfTheChapter.textColor = .black
            textOfTheBook.textColor = .black
        }
        else  {
            view.backgroundColor = .black
            backgroundIconsNormalizer()
            bgLowVisionColorButton.backgroundColor = .yellow
            titleOfTheChapter.backgroundColor = .black
            textOfTheBook.backgroundColor = .black
            titleOfTheChapter.textColor = .yellow
            textOfTheBook.textColor = .yellow
            
        }
        if counterLowVision <= 3 {
            speechUtterance(tappedWord: bgLowVisionColorButton.accessibilityLabel!)
        }
    }
    
    // Font style change functions
    
    @objc func tappedHelveticaFontButton(recognizer: UITapGestureRecognizer) {
        fontName = "Helvetica Neue"
        textOfTheBook.font = UIFont(name: fontName, size: adjuster)
        titleOfTheChapter.font = UIFont(name: fontName, size: adjuster)
        speechUtterance(tappedWord: fontToHelvetica.accessibilityLabel!)
    }
    @objc func tappedOpenDislexicFontButton() {
        fontName = "OpenDyslexic-Regular"
        textOfTheBook.font = UIFont(name: fontName, size: adjuster)
        titleOfTheChapter.font = UIFont(name: fontName, size: adjuster)
        speechUtterance(tappedWord: dyslexicFontButton.accessibilityLabel!)
    }
    
    // Line-spacing and left-margin functions
    
    @objc func tappedLineSpacer(){
        if size <= 10 {
            speechUtterance(tappedWord: lineSpacingButton.accessibilityLabel!)}
        normalizeLineSpacingChanges()
        
    }
    @objc func tappedLeftMarginButton(){
        if margin <= 60 {
            speechUtterance(tappedWord: leftMarginButton.accessibilityLabel!)
        }
        changeLeftMargin()
    }
    public func changeLeftMargin(){
        margin += 30
        if margin <= 120 {
            backgroundIconsNormalizer()
            textOfTheBook.frame = CGRect(x: margin, y: 210, width: 610 - margin, height: 460)
        }
        else {
            textOfTheBook.frame = CGRect(x: margin, y: 210, width: 610-margin, height: 460)
            if view.backgroundColor == .black || view.backgroundColor == #colorLiteral(red: 0.03921568627, green: 0.03921568627, blue: 0.03921568627, alpha: 1) {
                leftMarginButton.setBackgroundImage(#imageLiteral(resourceName: "resetMarginLeftWhite.png"), for: .normal)
            }
            else {
                leftMarginButton.setBackgroundImage(#imageLiteral(resourceName: "resetMarginLeftBlack.png"), for: .normal)
            }
            // reload
            margin = 40
        }
    }
    public func normalizeLineSpacingChanges(){
        if size == 30 {
            size += 5
            
            if view.backgroundColor == .black || view.backgroundColor == #colorLiteral(red: 0.03921568627, green: 0.03921568627, blue: 0.03921568627, alpha: 1) {
               lineSpacingButton.setBackgroundImage(#imageLiteral(resourceName: "resetLineSpacingWhite.png"), for: .normal)
            }
            else {
               lineSpacingButton.setBackgroundImage(#imageLiteral(resourceName: "resetLineSpacingBlack.png"), for: .normal)
            }
            lineSpacing(of: size)
            initialTextColorHolder()
            size = 0
        }
            
        else {
            backgroundIconsNormalizer()
            size += 5
            lineSpacing(of: size)
            initialTextColorHolder()
            
        }
    }
    public func lineSpacing(of size: CGFloat) {
        
        if size >= 30 {
            let style = NSMutableParagraphStyle()
            style.lineSpacing = size
            let attributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.paragraphStyle: style, .kern: wordSpacing]
            textOfTheBook.attributedText = NSAttributedString(string: textOfTheBook.text, attributes: attributes)
            titleOfTheChapter.attributedText = NSAttributedString(string: titleOfTheChapter.text, attributes: attributes)
            titleOfTheChapter.textAlignment = .center
            titleOfTheChapter.font = UIFont(name: fontName, size: adjuster)
        }
        else {
            let style = NSMutableParagraphStyle()
            style.lineSpacing = size
            let attributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.paragraphStyle: style, .kern: wordSpacing]
            textOfTheBook.attributedText = NSAttributedString(string: textOfTheBook.text, attributes: attributes)
            titleOfTheChapter.attributedText = NSAttributedString(string: titleOfTheChapter.text, attributes: attributes)
            titleOfTheChapter.textAlignment = .center
            titleOfTheChapter.font = UIFont(name: fontName, size: adjuster)
        }
    }
    
    // Text tap and double tap functions
    
    @objc func tapWordResponse(recognizer: UITapGestureRecognizer) {
        
        let location: CGPoint = recognizer.location(in: textOfTheBook)
        let position: CGPoint = CGPoint(x: location.x, y: location.y)
        let tapPosition: UITextPosition = textOfTheBook.closestPosition(to: position)!
        guard let textRange: UITextRange = textOfTheBook.tokenizer.rangeEnclosingPosition(tapPosition, with: UITextGranularity.word, inDirection: UITextDirection(rawValue: 3)) else {return}
        
        let tappedWord: String = textOfTheBook.text(in: textRange) ?? ""
        print("word to read ->", tappedWord)
        speechUtterance(tappedWord: tappedWord)
    }
    @objc func tapWordInTitleResponse(recognizer: UITapGestureRecognizer) {
        
        let location: CGPoint = recognizer.location(in: titleOfTheChapter)
        let position: CGPoint = CGPoint(x: location.x, y: location.y)
        let tapPosition: UITextPosition = titleOfTheChapter.closestPosition(to: position)!
        guard let textRange: UITextRange = titleOfTheChapter.tokenizer.rangeEnclosingPosition(tapPosition, with: UITextGranularity.word, inDirection: UITextDirection(rawValue: 3)) else {return}
        
        let tappedWord: String = titleOfTheChapter.text(in: textRange) ?? ""
        print("word to read ->", tappedWord)
        speechUtterance(tappedWord: tappedWord)
    }
    
    @objc func doubleTapTouch(recognizer: UITapGestureRecognizer){
           operationalControlHiding()
           let location: CGPoint = recognizer.location(in: textOfTheBook)
           let position: CGPoint = CGPoint(x: location.x, y: location.y)
           let tapPosition: UITextPosition = textOfTheBook.closestPosition(to: position)!
           guard let textRange: UITextRange = textOfTheBook.tokenizer.rangeEnclosingPosition(tapPosition, with: UITextGranularity.line, inDirection: UITextDirection(rawValue: 1)) else {return}
           let tappedWord: String = textOfTheBook.text(in: textRange) ?? ""
           print("line to read ->", tappedWord)
           let utterance = AVSpeechUtterance(string: tappedWord)
           utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
           utterance.rate = 0.3
           utterance.pitchMultiplier = 1.2
           speechSynthesizer.speak(utterance)
       }
   
    // Font size and word-spacing functions
    
    @objc func tappedIncreaseFontSize(){
        adjuster += 2
        speechUtterance(tappedWord: adjustFontButton.accessibilityLabel!)
        minimizeFontButton.backgroundColor = .none
        minimizeFontButton.isUserInteractionEnabled = true
        textOfTheBook.font = UIFont(name: fontName, size: adjuster)
        titleOfTheChapter.font = UIFont(name: fontName, size: adjuster)
        
        if adjuster >= 30 {
            adjustFontButton.isUserInteractionEnabled = false
            adjustFontButton.backgroundColor = .systemGray3
        }
    }
    @objc func tappedDecreaseFontSize(){
        adjuster -= 2
        speechUtterance(tappedWord: minimizeFontButton.accessibilityLabel!)
        adjustFontButton.isUserInteractionEnabled = true
        adjustFontButton.backgroundColor = .none
        textOfTheBook.font = UIFont(name: fontName, size: adjuster)
        titleOfTheChapter.font = UIFont(name: fontName, size: adjuster)
        if adjuster <= 12 {
            minimizeFontButton.isUserInteractionEnabled = false
            minimizeFontButton.backgroundColor = .systemGray3
        }
    }
    @objc func tappedWordSpacingIncreaseButton(){
        wordSpacing += 2
        
        speechUtterance(tappedWord: wordSpacingAdjustButton.accessibilityLabel!)
        spaceMinimizerButton.isUserInteractionEnabled = true
        spaceMinimizerButton.backgroundColor = .none
        initialTextColorHolder()
        if wordSpacing == 6 {
            wordSpacingAdjustButton.isUserInteractionEnabled = false
            wordSpacingAdjustButton.backgroundColor = .systemGray3
        }
        
    }
    @objc func tappedWordSpacingDecreaseButton(){
        wordSpacing -= 2
        
        speechUtterance(tappedWord: spaceMinimizerButton.accessibilityLabel!)
        wordSpacingAdjustButton.isUserInteractionEnabled = true
        wordSpacingAdjustButton.backgroundColor = .none
        initialTextColorHolder()
        
        if wordSpacing == -2{
            spaceMinimizerButton.isUserInteractionEnabled = false
            spaceMinimizerButton.backgroundColor = .systemGray3
        }
        
    }
    
    // Text & backgrounds normalizers + automatic controls hiding
    
    public func backgroundIconsNormalizer(){
        if view.backgroundColor == .black || view.backgroundColor == #colorLiteral(red: 0.03921568627, green: 0.03921568627, blue: 0.03921568627, alpha: 1) {
            settingsButton.setBackgroundImage(#imageLiteral(resourceName: "settingsWhite.png"), for: .normal)
            fontToHelvetica.setBackgroundImage(#imageLiteral(resourceName: "neuHelveticaWhite.png"), for: .normal)
            dyslexicFontButton.setBackgroundImage(#imageLiteral(resourceName: "dyslexiaWhite.png"), for: .normal)
            lineSpacingButton.setBackgroundImage(#imageLiteral(resourceName: "lineSpacingWhite.png"), for: .normal)
            leftMarginButton.setBackgroundImage(#imageLiteral(resourceName: "marginLeftWhite.png"), for: .normal)
            adjustFontButton.setBackgroundImage(#imageLiteral(resourceName: "adjustFontWhiteNormal.png"), for: .normal)
            minimizeFontButton.setBackgroundImage(#imageLiteral(resourceName: "reduceFontWhiteNormal.png"), for: .normal)
            spaceMinimizerButton.setBackgroundImage(#imageLiteral(resourceName: "reduceWordSpacingWhiteNormal.png"), for: .normal)
            wordSpacingAdjustButton.setBackgroundImage(#imageLiteral(resourceName: "adjustWordSpacingWhiteNormal.png"), for: .normal)
            adjustFontButton.setBackgroundImage(#imageLiteral(resourceName: "adjustFontWhite.png"), for: .highlighted)
            minimizeFontButton.setBackgroundImage(#imageLiteral(resourceName: "reduceFontWhite.png"), for: .highlighted)
            spaceMinimizerButton.setBackgroundImage(#imageLiteral(resourceName: "reduceWordSpacingWhite.png"), for: .highlighted)
            wordSpacingAdjustButton.setBackgroundImage(#imageLiteral(resourceName: "adjustWordSpacingWhite.png"), for: .highlighted)
            
        }
            
        else {
            settingsButton.setBackgroundImage(#imageLiteral(resourceName: "settingsBlack.png"), for: .normal)
            fontToHelvetica.setBackgroundImage(#imageLiteral(resourceName: "neuHelveticaBlack.png"), for: .normal)
            dyslexicFontButton.setBackgroundImage(#imageLiteral(resourceName: "dyslexiaBlack.png"), for: .normal)
            lineSpacingButton.setBackgroundImage(#imageLiteral(resourceName: "lineSpacingBlack.png"), for: .normal)
            leftMarginButton.setBackgroundImage(#imageLiteral(resourceName: "marginLeftBlack.png"), for: .normal)
            adjustFontButton.setBackgroundImage(#imageLiteral(resourceName: "adjustFontBlackNormal.png"), for: .normal)
            minimizeFontButton.setBackgroundImage(#imageLiteral(resourceName: "reduceFontBlackNormal.png"), for: .normal)
            spaceMinimizerButton.setBackgroundImage(#imageLiteral(resourceName: "reduceWordSpacingBlackNormal.png"), for: .normal)
            wordSpacingAdjustButton.setBackgroundImage(#imageLiteral(resourceName: "adjustWordSpacingNormal.png"), for: .normal)
            adjustFontButton.setBackgroundImage(#imageLiteral(resourceName: "adjustFontBlack.png"), for: .highlighted)
            minimizeFontButton.setBackgroundImage(#imageLiteral(resourceName: "reduceFontBlack.png"), for: .highlighted)
            spaceMinimizerButton.setBackgroundImage(#imageLiteral(resourceName: "reduceWordSpacingBlack.png"), for: .highlighted)
            wordSpacingAdjustButton.setBackgroundImage(#imageLiteral(resourceName: "adjustWordSpacingBlack.png"), for: .highlighted)
        }
    }
    public func initialTextColorHolder(){
        if textOfTheBook.textColor == .yellow {
        lineSpacing(of: size)
        textOfTheBook.font = UIFont(name: fontName, size: adjuster)
        textOfTheBook.textColor = .yellow
        titleOfTheChapter.textColor = .yellow
        }
        else if textOfTheBook.textColor == .white || textOfTheBook.textColor == #colorLiteral(red: 1, green: 1, blue: 0.8980392157, alpha: 1) {
            lineSpacing(of: size)
            textOfTheBook.font = UIFont(name: fontName, size: adjuster)
            textOfTheBook.textColor = .white
            titleOfTheChapter.textColor = .white
        }
         
        else{
            lineSpacing(of: size)
            textOfTheBook.font = UIFont(name: fontName, size: adjuster)
        }
    }
    public func operationalControlHiding(){
        wordSpacingAdjustButton.isHidden = true
        adjustFontButton.isHidden = true
        spaceMinimizerButton.isHidden = true
        minimizeFontButton.isHidden = true
        fontToHelvetica.isHidden = true
        dyslexicFontButton.isHidden = true
        bgWhiteOnBlackColorButton.isHidden = true
        bgPastelColorButton.isHidden = true
        bgLowVisionColorButton.isHidden = true
        lineSpacingButton.isHidden = true
        leftMarginButton.isHidden = true
    }
    
}

// Speech settings and Synthesizer

extension InitialViewController: AVSpeechSynthesizerDelegate{
    
    public func speechUtterance(tappedWord: String) {
        let utterance = AVSpeechUtterance(string: tappedWord)
        utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
        utterance.rate = 0.4
        
        let synth = AVSpeechSynthesizer()
        
        synth.speak(utterance)
    }
    public func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance) {
        let mutableAttributedString = NSMutableAttributedString(string: utterance.speechString)
        mutableAttributedString.addAttribute(.foregroundColor, value: UIColor.white, range: characterRange)
        mutableAttributedString.addAttribute(.backgroundColor, value: UIColor.blue, range: characterRange)
        
        titleOfTheChapter.attributedText = mutableAttributedString
        
        titleOfTheChapter.font = UIFont.preferredFont(forTextStyle: .title1)
        titleOfTheChapter.font = UIFont(name: fontName, size: adjuster)
        if textOfTheBook.textColor == .white || textOfTheBook.textColor == #colorLiteral(red: 1, green: 1, blue: 0.8980392157, alpha: 1) || textOfTheBook.textColor == .yellow  {
            titleOfTheChapter.textColor = .white
            
        } else {
        }
        titleOfTheChapter.textAlignment = .center
        print("speaking")
    }
    public func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        titleOfTheChapter.attributedText = NSAttributedString(string: utterance.speechString)
        titleOfTheChapter.text = books.chapterOfTheBook[0]
        titleOfTheChapter.font = UIFont.preferredFont(forTextStyle: .headline)
        titleOfTheChapter.font = UIFont(name: fontName, size: adjuster)
        initialTextColorHolder()
        titleOfTheChapter.textAlignment = .center
        print("finished")
    }
    public override func viewDidLoad() {
        super.viewDidLoad()
        speechSynthesizer.delegate = self
    }
}
