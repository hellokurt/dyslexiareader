//Gleb Losev

import UIKit
import PlaygroundSupport

let viewController = InitialViewController()
viewController.preferredContentSize = CGSize(width: 700, height: 700)
PlaygroundPage.current.liveView = viewController


/**
# Use command and command + shift to tap with two fingers on a line.
*/

/**
 #In the app I've used:
 #1. An excerpt from Lewis Carroll. `Alice's Adventures in Wonderland`. Apple Books. (https://books.apple.com/ru/book/alices-adventures-in-wonderland/id510986661?l=en)
 #2. A setting icon (https://icons8.com/icon/54151/gear) from Icons8 website, where I have a paid account.
 #3. Dyslexia Open font by https://opendyslexic.org/
 #4. Style requerements by British Dislex!a Association(https://www.bdadyslexia.org.uk/advice/employers/creating-a-dyslexia-friendly-workplace/dyslexia-friendly-style-guide) #5. Researches and articles about Low Vision and Dyslexia:
 #* Good Background Colors for Readers: A Study of People with and without Dyslexia (https://www.cs.cmu.edu/~jbigham/pubs/pdfs/2017/colors.pdf)
 #* Optimal Colors To Improve Readability For People With Dyslexia (https://www.w3.org/WAI/RD/2012/text-customization/r11)
 #* Accessibility Requirements for People with Low Vision (https://www.w3.org/TR/low-vision-needs/)
 (https://venngage.com/blog/color-blind-friendly-palette/)
 #* Web accessibility and people with dyslexia: A survey on techniques and guidelines  (https://www.researchgate.net/publication/239761585_Web_accessibility_and_people_with_dyslexia_A_survey_on_techniques_and_guidelines)
 #* Layout Guidelines for Web Text and a Web Service  to Improve Accessibility for Dyslexics (https://www.researchgate.net/publication/239761586_Layout_guidelines_for_web_text_and_a_web_service_to_improve_accessibility_for_dyslexics)
 #* Background chromatic contrast preference in cases with age-related macular degeneration (https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3880533/)
*/


